# Advanced Git Workshop
Lab 05: Stashing Changes

---

# Tasks

 - Create some local changes
 
 - Stash your local changes
 
 - Manage your stashes

---

## Preparations

 - Let's clone a repository to work with:
 
```
$ git clone https://oauth2:gkLjhh5W4te-cJngRBxB@gitlab.com/sela-git-advanced-workshop/static-web-app.git lab5
$ cd lab5
```

---

## Create some local changes

  - Edit the index.html file and add it to the index areas:
```
$ echo first update >> index.html
$ git add index.html
```

 - Create a new file with some content (don't add it to the index area)
```
$ touch new-file.txt
$ echo some content > new-file.txt
```

 - Check the repository status 
```
$ git status
```

## Stash your local changes

 - Let's Checkout the feature branch to perform a change there (you will get an error): 
```
$ git checkout feature
```
```
error: Your local changes to the following files would be overwritten by checkout:
        index.html
Please commit your changes or stash them before you switch branches.
Aborting
```

 - Let's stash the changes to be able to checkout the feature branch:
```
$ git stash
```

 - Check the repository status
```
$ git status
```

 - Oh, we forget add the untracked changes. Let's re-establish the changes and recreate the stash:
```
$ git stash apply
$ git stash --include-untracked
```

 - Now you should be able to checkout the feature branch: 
```
$ git checkout feature
```

---

## Manage your stashes

- Let's go back to the master branch:
```
$ git checkout master
```

 - Check the repository status
```
$ git status
```

 - See the existent stashes
```
$ git stash list
```

 - Delete the first stash created before:
```
$ git stash drop stash@{1}
```

 - See the existent stashes
```
$ git stash list
```

 - See the second stash details (note only tracked files are shown):
```
$ git stash show stash@{0}
$ git stash show -p stash@{0}
```

 - Unstash the changes using
```
$ git stash apply stash@{0}
```

 - Check the repository status
```
$ git status
```

 - Clear all the stashes
```
$ git stash clear
```

---

# Cleanup (optional)

 - Remove the repository used during the lab:
 
```
$ cd ..
$ rm -rf lab5
```

